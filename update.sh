#!/bin/bash

echo "Updating hosts..."
wget --show-progress -O hosts -- https://github.com/StevenBlack/hosts/raw/master/hosts && /etc/init.d/dnsmasq restart
echo "Update finished. "